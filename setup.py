import os
from setuptools import setup

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='nirvaris-profile',
    version='3.1.2',
    packages=['n_profile'],
    include_package_data=True,
    license='MIT License',  # example license
    description='A simple Django app using django auth with custom UI',
    long_description=README,
    url='https://gitlab.com/nirvaris-stuff/nirvaris-profile',
    author='Nirvaris',
    author_email='contact@nirvaris.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License', # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.x',
    ],

)
